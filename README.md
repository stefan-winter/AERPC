# The Artifact Evaluations Recommended Practices Compendium (AERPC)

Artifact evaluations complement peer reviews for research articles with peer reviews for research artifacts.
Most artifact evaluation processes are based on the framework provided by [ACM's guidelines](https://www.acm.org/publications/policies/artifact-review-and-badging-current).
These guidelines are rather generic and encourage individual communities or conferences to establish their own guidelines and processes in line with the more general framework.
As a consequence, a large variety of AE processes have evolved independently.
While this has led to many good processes and practices, their separate evolution is limiting cross-community exchange on which practices work well and which ones don't.
This repository is intended to serve as a compendium of recommended (and also discouraged) practices for AE based on the experiences of AEC chairs and AEC members.
We welcome contributions!
For now, the easiest way to contribute is to open an issue.
We will continuously streamline the contribution processes.
